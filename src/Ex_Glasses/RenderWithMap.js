import React, { Component } from "react";
import { dataGlasses } from "./dataGlasses";

class RenderWithMap extends Component {
  state = {
    url: "./glassesImage/v1.png",
    name: "GUCCI G8850U",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  };
  /* Rendr lai khi click */
  carryGlasses = (id) => {
    dataGlasses.map((item) => {
      if (item.id == id) {
        this.setState({
          url: `${item.url}`,
          name: `${item.name}`,
          desc: `${item.desc}`,
        });
      }
    });
  };
  /* Render lan dau */
  renderListGlasses = () => {
    return dataGlasses.map((item) => {
      return (
        <div
          id={item.id}
          onClick={() => {
            this.carryGlasses(`${item.id}`);
          }}
        >
          <img src={item.url} alt="" />
        </div>
      );
    });
  };

  render() {
    return (
      <div className="myBody">
        <div className="model">
          <div className="img">
            <img src="./glassesImage/model.jpg" />
            <img className="glasses__img" src={this.state.url} />
            <div className="info">
              <h4>{this.state.name}</h4>
              <p>{this.state.desc}</p>
            </div>
          </div>
          <div className="img">
            <img src="./glassesImage/model.jpg" />
          </div>
        </div>
        <div className="glasses">{this.renderListGlasses()}</div>
      </div>
    );
  }
}

export default RenderWithMap;
