import React, { Component } from "react";
import RenderWithMap from "./RenderWithMap";

class Ex_Glasses_layout extends Component {
  render() {
    return (
      <div className="glasses__layout">
        <div className="header">Try glasses app online</div>

        <RenderWithMap />

        <div className="glasses__layout-overlay"></div>
      </div>
    );
  }
}

export default Ex_Glasses_layout;
